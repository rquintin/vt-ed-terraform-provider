package vted

import (
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"github.com/tidwall/gjson"
)

func dataSourceVtEdGroup() *schema.Resource {
	return &schema.Resource{
		Read: dataSourceVtEdGroupRead,
		Schema: map[string]*schema.Schema{
			"name": {
				Type:        schema.TypeString,
				Description: "The name of the group (same as uugid)",
				Required:    true,
			},
			"uugid": {
				Type:        schema.TypeString,
				Description: "The uuid of the group",
				Computed:    true,
			},
			"suppress_display": {
				Type:        schema.TypeBool,
				Description: "Is this group suppressed from being displayed in public listings?",
				Computed:    true,
			},
			"suppress_members": {
				Type:        schema.TypeBool,
				Description: "Is this group's membership suppressed from being displayed in public listings?",
				Computed:    true,
			},
			"replication_targets": {
				Type:        schema.TypeList,
				Description: "Replicate group to downstream targets. Available targets: AD, GIT, GOOG",
				MinItems:    0,
				Computed:    true,
				Elem:        schema.TypeString,
			},
			"administrators": {
				Type:        schema.TypeList,
				Description: "The administrators of the group",
				MinItems:    1,
				Computed:    true,
				Elem:        PersonResource(),
			},
			"contacts": {
				Type:        schema.TypeList,
				Description: "The contacts of the group",
				MinItems:    1,
				Computed:    true,
				Elem:        PersonResource(),
			},
			"managers": {
				Type:        schema.TypeList,
				Description: "The managers of the group",
				MinItems:    0,
				Computed:    true,
				Elem:        PersonResource(),
			},
			"members": {
				Type:        schema.TypeList,
				Description: "The members of the group",
				MinItems:    1,
				Computed:    true,
				Elem:        PersonResource(),
			},
			"viewers": {
				Type:        schema.TypeList,
				Description: "The viewers of the group",
				Computed:    true,
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						"kind": {
							Type:        schema.TypeString,
							Description: "The type of viewer",
							Computed:    true,
						},
						"uusid": {
							Type:        schema.TypeString,
							Description: "The uusid of the viewer",
							Computed:    true,
						},
					},
				},
			},
		},
	}
}

func dataSourceVtEdGroupRead(d *schema.ResourceData, meta interface{}) error {
	providerConfig := meta.(*ProviderConfig)
	edClient := providerConfig.client

	data, err := edClient.Get("/groups/" + d.Get("name").(string) + "?with=all")
	if err != nil {
		return err
	}

	d.Set("uugid", data.Get("uugid").String())
	d.Set("name", data.Get("uugid").String())
	d.Set("suppress_display", data.Get("suppressDisplay").Bool())
	d.Set("suppress_members", data.Get("suppressMembers").Bool())
	d.Set("replication_targets", data.Get("targets").Array())
	d.Set("administrators", convertPeople(data.Get("administrators").Array()))
	d.Set("contacts", convertPeople(data.Get("contacts").Array()))
	d.Set("managers", convertPeople(data.Get("managers").Array()))
	d.Set("members", convertPeople(data.Get("members").Array()))
	d.Set("viewers", convertViewers(data.Get("viewers").Array()))

	d.SetId(data.Get("uugid").String())

	return nil
}

func convertPeople(people []gjson.Result) (values []map[string]interface{}) {
	for _, person := range people {
		v := map[string]interface{}{
			"display_name": person.Get("displayName").String(),
			"pid":          person.Get("pid").String(),
			"uid":          person.Get("uid").Int(),
		}
		values = append(values, v)
	}
	return values
}

func convertViewers(viewers []gjson.Result) (values []map[string]interface{}) {
	for _, viewer := range viewers {
		v := map[string]interface{}{
			"kind":  viewer.Get("kind").String(),
			"uusid": viewer.Get("uusid").String(),
		}
		values = append(values, v)
	}
	return values
}
