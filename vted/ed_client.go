package vted

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"github.com/tidwall/gjson"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

var httpClient *http.Client

type EdClient struct {
	baseUrl    string
	httpClient *http.Client
}

func NewEdClient(baseUrl string, caCertKeyPath string, privateKeyPath string, publicKeyPath string) *EdClient {
	client := new(EdClient)
	client.baseUrl = baseUrl
	client.httpClient = createHttpClient(caCertKeyPath, privateKeyPath, publicKeyPath)
	return client
}

func (e *EdClient) Get(url string) (*gjson.Result, error) {
	resp, err := e.httpClient.Get(e.baseUrl + url)
	if err != nil {
		return nil, err
	}

	data, err := handleResponse(resp)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (e *EdClient) Post(url string, body string) (*gjson.Result, error) {
	resp, err := e.httpClient.Post(e.baseUrl+url, "application/x-www-form-urlencoded", strings.NewReader(body))
	if err != nil {
		return nil, err
	}

	data, err := handleResponse(resp)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (e *EdClient) Delete(url string) (*gjson.Result, error) {
	req, err := http.NewRequest(http.MethodDelete, e.baseUrl+url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := e.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	data, err := handleResponse(resp)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (e *EdClient) Patch(url string, patch string) (*gjson.Result, error) {
	req, err := http.NewRequest(http.MethodPatch, e.baseUrl+url, strings.NewReader(patch))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		return nil, err
	}

	resp, err := e.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	data, err := handleResponse(resp)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func createHttpClient(caCertPath string, privateKeyPath string, publicKeyPath string) *http.Client {
	caCert, _ := ioutil.ReadFile(caCertPath)
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	cert, _ := tls.LoadX509KeyPair(publicKeyPath, privateKeyPath)

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs:      caCertPool,
				Certificates: []tls.Certificate{cert},
			},
		},
	}

	return client
}

func handleResponse(response *http.Response) (*gjson.Result, error) {
	json, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	if response.StatusCode != 200 && response.StatusCode != 201 {
		log.Printf("[WARN] received status code %d from server with response: %s", response.StatusCode, string(json))
		return nil, errors.New(fmt.Sprintf("received error: %s", string(json)))
	}

	jsonData := gjson.ParseBytes(json)
	return &jsonData, nil
}
