package vted

import (
	"errors"
	"fmt"
	"log"
	"net/url"
	"strings"
	"time"

	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"github.com/tidwall/gjson"
)

func resourceVtEdGroup() *schema.Resource {
	return &schema.Resource{
		Create:   resourceVtEdGroupCreate,
		Read:     resourceVtEdGroupRead,
		Update:   resourceVtEdGroupUpdate,
		Delete:   resourceVtEdGroupDelete,
		Importer: resourceVtEdGroupImporter(),

		Schema: map[string]*schema.Schema{
			"name": &schema.Schema{
				Type:        schema.TypeString,
				Description: "The name/uugid of the group",
				Required:    true,
			},
			"creation_date": &schema.Schema{
				Type:        schema.TypeString,
				Description: "Creation date, in ISO 8601 format",
				Computed:    true,
			},
			"expiration_date": &schema.Schema{
				Type:             schema.TypeString,
				Description:      "Expiration date, in ISO 8601 format",
				Optional:         true,
				Computed:         true,
				DiffSuppressFunc: suppressTimeChange(),
			},
			"suppress_members": &schema.Schema{
				Type:        schema.TypeBool,
				Description: "Are members available publicly?",
				Optional:    true,
				Default:     true,
			},
			"suppress_display": &schema.Schema{
				Type:        schema.TypeBool,
				Description: "Is the group available publicly?",
				Optional:    true,
				Default:     true,
			},
			"replication_targets": &schema.Schema{
				Type:        schema.TypeSet,
				Description: "Replicate group to downstream targets. Available targets: AD, GIT, GOOG",
				Optional:    true,
				Set:         schema.HashString,
				Elem:        &schema.Schema{Type: schema.TypeString},
			},
			"admins": &schema.Schema{
				Type:        schema.TypeList,
				Description: "Administrators for the group",
				Required:    true,
				MinItems:    1,
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						"people": {
							Type:        schema.TypeSet,
							Description: "PIDs of the person administrators of the group",
							Optional:    true,
							Set:         schema.HashString,
							Elem:        &schema.Schema{Type: schema.TypeString},
						},
						"services": {
							Type:        schema.TypeSet,
							Description: "UUSIDs of the service administrators of the group",
							Optional:    true,
							Set:         schema.HashString,
							Elem:        &schema.Schema{Type: schema.TypeString},
						},
					},
				},
			},
			"contacts": &schema.Schema{
				Type:        schema.TypeSet,
				Description: "PIDs of contacts for the group",
				Required:    true,
				MinItems:    1,
				Set:         schema.HashString,
				Elem:        &schema.Schema{Type: schema.TypeString},
			},
			"managers": &schema.Schema{
				Type:        schema.TypeList,
				Description: "Managers for the group",
				Optional:    true,
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						"people": {
							Type:        schema.TypeSet,
							Description: "PIDs of the person managers of the group",
							Optional:    true,
							Set:         schema.HashString,
							Elem:        &schema.Schema{Type: schema.TypeString},
						},
						"services": {
							Type:        schema.TypeSet,
							Description: "UUSIDs of the service managers of the group",
							Optional:    true,
							Set:         schema.HashString,
							Elem:        &schema.Schema{Type: schema.TypeString},
						},
					},
				},
			},
			"members": &schema.Schema{
				Type:        schema.TypeList,
				Description: "Membership information",
				Optional:    true,
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						"people": {
							Type:        schema.TypeSet,
							Description: "PIDs of the members of the group",
							Optional:    true,
							Set:         schema.HashString,
							Elem:        &schema.Schema{Type: schema.TypeString},
						},
						"groups": {
							Type:        schema.TypeSet,
							Description: "PIDs of the members of the group",
							Optional:    true,
							Set:         schema.HashString,
							Elem:        &schema.Schema{Type: schema.TypeString},
						},
					},
				},
			},
			"viewers": &schema.Schema{
				Type:        schema.TypeSet,
				Description: "SIDs of service viewers for the group",
				Optional:    true,
				Set:         schema.HashString,
				Elem:        &schema.Schema{Type: schema.TypeString},
			},
		},
	}
}

func resourceVtEdGroupCreate(d *schema.ResourceData, meta interface{}) error {
	providerConfig := meta.(*ProviderConfig)
	edClient := providerConfig.client

	groupName := d.Get("name").(string)

	err := validateAdminsAndContacts(d.Get("admins").([]interface{}), d.Get("contacts").(*schema.Set))
	if err != nil {
		return err
	}
	// provider service ID always gets added as a service admin for the group
	d.Get("admins").([]interface{})[0].(map[string]interface{})["services"].(*schema.Set).Add(providerConfig.serviceId)

	_, err = edClient.Post("/groups", groupConfigToFormBody(d))
	if err != nil {
		return err
	}

	d.SetId(groupName)

	err = applyPatchesWhereNecessary(edClient, d)
	if err != nil {
		return err
	}

	err = syncManagersAdmins(groupName, "managers", d.Get("managers").([]interface{}), nil, edClient)
	if err != nil {
		return err
	}

	err = syncManagersAdmins(groupName, "administrators", d.Get("admins").([]interface{}), nil, edClient)
	if err != nil {
		return err
	}

	err = syncMembers(groupName, d.Get("members").([]interface{}), nil, edClient)
	if err != nil {
		return err
	}

	err = syncReplicationTargets(groupName, d.Get("replication_targets").(*schema.Set), nil, edClient)
	if err != nil {
		return err
	}

	// Services always get added as viewers to the groups they created
	//d.Get("viewers").(*schema.Set).Add(providerConfig.serviceId)
	err = syncRole(groupName, "viewers", d.Get("viewers").(*schema.Set), nil, edClient, "service")
	if err != nil {
		return err
	}

	return resourceVtEdGroupRead(d, meta)
}

func syncManagersAdmins(groupName string, roleName string, newCollection []interface{}, oldCollection []interface{}, client *EdClient) error {
	var newPeopleSet *schema.Set
	var oldPeopleSet *schema.Set
	var newServicesSet *schema.Set
	var oldServicesSet *schema.Set

	if len(newCollection) > 0 && newCollection[0] != nil {
		newMembers := newCollection[0].(map[string]interface{})
		newPeopleSet = newMembers["people"].(*schema.Set)
		newServicesSet = newMembers["services"].(*schema.Set)
	} else {
		newPeopleSet = schema.NewSet(schema.HashString, []interface{}{})
		newServicesSet = schema.NewSet(schema.HashString, []interface{}{})
	}

	if len(oldCollection) > 0 {
		oldMembers := oldCollection[0].(map[string]interface{})
		oldPeopleSet = oldMembers["people"].(*schema.Set)
		oldServicesSet = oldMembers["services"].(*schema.Set)
	} else {
		oldPeopleSet = schema.NewSet(schema.HashString, []interface{}{})
		oldServicesSet = schema.NewSet(schema.HashString, []interface{}{})
	}

	err := syncRole(groupName, roleName, newPeopleSet, oldPeopleSet, client, "person")
	if err != nil {
		return err
	}

	err = syncRole(groupName, roleName, newServicesSet, oldServicesSet, client, "service")
	if err != nil {
		return err
	}

	return nil
}

func syncMembers(groupName string, newMemberCollection []interface{}, oldMemberCollection []interface{}, client *EdClient) error {
	var newPeopleSet *schema.Set
	var oldPeopleSet *schema.Set
	var newGroupSet *schema.Set
	var oldGroupSet *schema.Set

	if len(newMemberCollection) > 0 {
		newMembers := newMemberCollection[0].(map[string]interface{})
		newPeopleSet = newMembers["people"].(*schema.Set)
		newGroupSet = newMembers["groups"].(*schema.Set)
	} else {
		newPeopleSet = schema.NewSet(schema.HashString, []interface{}{})
		newGroupSet = schema.NewSet(schema.HashString, []interface{}{})
	}

	if len(oldMemberCollection) > 0 {
		oldMembers := oldMemberCollection[0].(map[string]interface{})
		oldPeopleSet = oldMembers["people"].(*schema.Set)
		oldGroupSet = oldMembers["groups"].(*schema.Set)
	} else {
		oldPeopleSet = schema.NewSet(schema.HashString, []interface{}{})
		oldGroupSet = schema.NewSet(schema.HashString, []interface{}{})
	}

	err := syncRole(groupName, "members", newPeopleSet, oldPeopleSet, client, "person")
	if err != nil {
		return err
	}

	err = syncRole(groupName, "members", newGroupSet, oldGroupSet, client, "group")
	if err != nil {
		return err
	}

	return nil
}

func resourceVtEdGroupRead(d *schema.ResourceData, meta interface{}) error {
	providerConfig := meta.(*ProviderConfig)
	edClient := providerConfig.client

	groupInfo, err := edClient.Get("/groups/" + d.Id() + "?with=all")
	if err != nil {
		return err
	}

	d.Set("expiration_date", groupInfo.Get("expirationDate").String())
	d.Set("creation_date", groupInfo.Get("creationDate").String())
	d.Set("suppress_members", groupInfo.Get("suppressMembers").Bool())
	d.Set("suppress_display", groupInfo.Get("suppressDisplay").Bool())
	d.Set("admins", convertManagersAdmins(groupInfo.Get("administrators").Array()))
	d.Set("replication_targets", groupInfo.Get("targets").Array())
	d.Set("contacts", convertFlattenedPeople(groupInfo.Get("contacts").Array()))
	d.Set("managers", convertManagersAdmins(groupInfo.Get("managers").Array()))
	d.Set("viewers", convertViewers(groupInfo.Get("viewers").Array()))
	d.Set("members", convertMembers(groupInfo.Get("members").Array()))

	return nil
}

func resourceVtEdGroupUpdate(d *schema.ResourceData, meta interface{}) error {
	providerConfig := meta.(*ProviderConfig)
	edClient := providerConfig.client

	groupName := d.Get("name").(string)

	err := applyPatchesWhereNecessary(edClient, d)
	if err != nil {
		return err
	}

	if d.HasChange("replication_targets") {
		oldValue, newValue := d.GetChange("replication_targets")
		err := syncReplicationTargets(groupName, newValue.(*schema.Set), oldValue.(*schema.Set), edClient)
		if err != nil {
			return err
		}
	}

	// provider service ID always gets added as a service admin for the group
	d.Get("admins").([]interface{})[0].(map[string]interface{})["services"].(*schema.Set).Add(providerConfig.serviceId)

	if d.HasChange("admins") {
		oldValue, newValue := d.GetChange("admins")
		err := syncManagersAdmins(groupName, "administrators", newValue.([]interface{}), oldValue.([]interface{}), edClient)
		if err != nil {
			return err
		}
	}

	if d.HasChange("contacts") {
		oldValue, newValue := d.GetChange("contacts")
		err := syncRole(groupName, "contacts", newValue.(*schema.Set), oldValue.(*schema.Set), edClient, "person")
		if err != nil {
			return err
		}
	}

	if d.HasChange("managers") {
		oldValue, newValue := d.GetChange("managers")
		err := syncManagersAdmins(groupName, "managers", newValue.([]interface{}), oldValue.([]interface{}), edClient)
		if err != nil {
			return err
		}
	}

	if d.HasChange("members") {
		oldValue, newValue := d.GetChange("members")
		err := syncMembers(groupName, newValue.([]interface{}), oldValue.([]interface{}), edClient)
		if err != nil {
			return err
		}
	}

	// provider service ID always gets added as a service viewer for the group
	d.Get("viewers").(*schema.Set).Add(providerConfig.serviceId)
	if d.HasChange("viewers") {
		oldValue, newValue := d.GetChange("viewers")
		err := syncRole(groupName, "viewers", newValue.(*schema.Set), oldValue.(*schema.Set), edClient, "service")
		if err != nil {
			return err
		}
	}

	return resourceVtEdGroupRead(d, meta)
}

func resourceVtEdGroupDelete(d *schema.ResourceData, meta interface{}) error {
	providerConfig := meta.(*ProviderConfig)
	edClient := providerConfig.client

	_, err := edClient.Delete("/groups/" + d.Get("name").(string))
	if err != nil {
		return err
	}

	return nil
}

func resourceVtEdGroupImporter() *schema.ResourceImporter {
	return &schema.ResourceImporter{
		State: func(data *schema.ResourceData, meta interface{}) ([]*schema.ResourceData, error) {
			err := resourceVtEdGroupRead(data, meta)
			if err != nil {
				return nil, err
			}
			return []*schema.ResourceData{data}, nil
		},
	}
}

func suppressTimeChange() func(k string, old string, new string, d *schema.ResourceData) bool {
	return func(k, old, new string, d *schema.ResourceData) bool {
		oldTime, err := parseDateString(old)
		if err != nil {
			return false
		}
		newTime, err := parseDateString(new)
		if err != nil {
			return false
		}

		return oldTime.Equal(newTime)
	}
}

func validateAdminsAndContacts(admins []interface{}, contacts *schema.Set) error {
	var adminPeopleSet *schema.Set
	var adminServicesSet *schema.Set

	adminsAndContacts := make(map[string]bool)

	if len(admins) > 0 && admins[0] != nil {
		adminMap := admins[0].(map[string]interface{})
		adminPeopleSet = adminMap["people"].(*schema.Set)
		adminServicesSet = adminMap["services"].(*schema.Set)
		for _, n := range adminPeopleSet.List() {
			adminsAndContacts[n.(string)] = true
		}
		for _, n := range adminServicesSet.List() {
			adminsAndContacts[n.(string)] = true
		}
	}

	for _, n := range contacts.List() {
		adminsAndContacts[n.(string)] = true
	}

	if len(adminsAndContacts) <= 1 {
		return errors.New("two distinct users are required for admins and contacts")
	}

	return nil
}

// Creation only supports specifying uugid, admins, and contacts
func groupConfigToFormBody(d *schema.ResourceData) string {
	formDataObj := url.Values{}
	formDataObj.Set("uugid", d.Get("name").(string))

	// Creation only supports people admins
	adminMap := d.Get("admins").([]interface{})[0].(map[string]interface{})
	for _, name := range adminMap["people"].(*schema.Set).List() {
		formDataObj.Add("administrator", name.(string))
	}

	for _, name := range d.Get("contacts").(*schema.Set).List() {
		formDataObj.Add("contact", name.(string))
	}

	return formDataObj.Encode()
}

func applyPatchesWhereNecessary(edClient *EdClient, d *schema.ResourceData) error {
	var patches []string

	expirationDate := d.Get("expiration_date")
	if (d.IsNewResource() && expirationDate != "") || d.HasChange("expiration_date") {
		parsedDate, err := parseDateString(expirationDate.(string))
		if err != nil {
			return err
		}
		patches = append(patches, fmt.Sprintf(`{"op":"replace","path":"/expirationDate","value":%d}`, parsedDate.Unix()))
	}

	// Defaults for suppression is true, so only run if explicitly disabling
	suppressMembers := d.Get("suppress_members")
	if (d.IsNewResource() && suppressMembers == false) || d.HasChange("suppress_members") {
		patches = append(patches, fmt.Sprintf(`{"op":"replace","path":"/suppressMembers","value":%t}`, suppressMembers.(bool)))
	}

	suppressDisplay := d.Get("suppress_display")
	if (d.IsNewResource() && suppressDisplay == false) || d.HasChange("suppress_display") {
		patches = append(patches, fmt.Sprintf(`{"op":"replace","path":"/suppressDisplay","value":%t}`, suppressDisplay.(bool)))
	}

	if len(patches) > 0 {
		patch := "[" + strings.Join(patches, ",") + "]"
		log.Println("[DEBUG] Making a patch with the following: " + patch)
		_, err := edClient.Patch("/groups/"+d.Get("name").(string), patch)
		if err != nil {
			return err
		}
	}

	return nil
}

func syncReplicationTargets(groupName string, newMembers *schema.Set, oldMembers *schema.Set, client *EdClient) error {
	var targetsToAdd []interface{}
	if oldMembers == nil {
		targetsToAdd = newMembers.List()
	} else {
		targetsToAdd = newMembers.Difference(oldMembers).List()
	}

	postUrl := fmt.Sprintf("/groups/%s/targets", groupName)
	for _, n := range targetsToAdd {
		formDataObj := url.Values{}
		formDataObj.Set("system", n.(string))
		_, err := client.Post(postUrl, formDataObj.Encode())
		if err != nil {
			return err
		}
	}

	var targetsToRemove []interface{}
	if oldMembers == nil {
		targetsToRemove = []interface{}{}
	} else {
		targetsToRemove = oldMembers.Difference(newMembers).List()
	}

	for _, n := range targetsToRemove {
		log.Printf("[DEBUG] Removing replication target '%s' from group '%s'", n.(string), groupName)
		deleteUrl := fmt.Sprintf("/groups/%s/targets/%s", groupName, n.(string))
		_, err := client.Delete(deleteUrl)
		if err != nil {
			return err
		}
	}

	return nil
}

func syncRole(groupName string, roleName string, newMembers *schema.Set, oldMembers *schema.Set, client *EdClient, memberType string) error {
	var membersToAdd []interface{}
	if oldMembers == nil {
		membersToAdd = newMembers.List()
	} else {
		membersToAdd = newMembers.Difference(oldMembers).List()
	}

	postUrl := fmt.Sprintf("/groups/%s/%s", groupName, roleName)
	for _, n := range membersToAdd {
		formDataObj := url.Values{}
		formDataObj.Set("kind", memberType)
		formDataObj.Set("id", n.(string))
		_, err := client.Post(postUrl, formDataObj.Encode())
		if err != nil {
			return err
		}
	}

	var membersToRemove []interface{}
	if oldMembers == nil {
		membersToRemove = []interface{}{}
	} else {
		membersToRemove = oldMembers.Difference(newMembers).List()
	}

	for _, n := range membersToRemove {
		log.Printf("[DEBUG] Removing '%s' from role '%s' from group '%s'", n.(string), roleName, groupName)
		deleteUrl := fmt.Sprintf("/groups/%s/%s/%s", groupName, roleName, n.(string))
		_, err := client.Delete(deleteUrl)
		if err != nil {
			return err
		}
	}

	return nil
}

func convertFlattenedPeople(people []gjson.Result) (values []string) {
	for _, person := range people {
		if person.Get("kind").String() == "person" {
			values = append(values, person.Get("pid").String())
		}
		if person.Get("kind").String() == "service" {
			values = append(values, person.Get("uusid").String())
		}
	}
	return values
}

func parseDateString(date string) (time.Time, error) {
	if date == "" {
		return time.Now(), nil
	}

	parsedDate, err := time.Parse(time.RFC3339, date)
	if err != nil {
		parsedDate, err = time.Parse("2006-01-02", date)
		if err != nil {
			return time.Now(), errors.New(fmt.Sprintf("error parsing time %s. Needs to be in RFC3339 or YYYY-MM-DD format"))
		}
	}
	return parsedDate, nil
}

func convertManagersAdmins(members []gjson.Result) []map[string]*schema.Set {
	people := schema.NewSet(schema.HashString, []interface{}{})
	services := schema.NewSet(schema.HashString, []interface{}{})

	for _, member := range members {
		memberKind := member.Get("kind").String()
		if memberKind == "person" {
			people.Add(member.Get("pid").String())
		} else if memberKind == "service" {
			services.Add(member.Get("uusid").String())
		}
	}

	values := map[string]*schema.Set{
		"people":   people,
		"services": services,
	}

	return []map[string]*schema.Set{
		values,
	}
}

func convertMembers(members []gjson.Result) []map[string]*schema.Set {
	people := schema.NewSet(schema.HashString, []interface{}{})
	groups := schema.NewSet(schema.HashString, []interface{}{})

	for _, member := range members {
		memberKind := member.Get("kind").String()
		if memberKind == "person" {
			people.Add(member.Get("pid").String())
		} else if memberKind == "group" {
			groups.Add(member.Get("uugid").String())
		}
	}

	values := map[string]*schema.Set{
		"people": people,
		"groups": groups,
	}

	return []map[string]*schema.Set{
		values,
	}
}
