FROM golang AS builder
WORKDIR /build
COPY go.mod go.sum ./
RUN go mod download
COPY build.sh ./
COPY main.go ./
COPY vted ./vted
RUN ./build.sh

FROM scratch
COPY --from=builder /build/build/ .
CMD ["/terraform-provider-vted-linux-amd64"]