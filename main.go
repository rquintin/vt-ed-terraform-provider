package main

import (
	"encoding/json"
	"github.com/hashicorp/terraform-plugin-sdk/plugin"
	vted "github.com/terraform-provider-vt-ed-group/vted"
	"log"
)

func main() {
	plugin.Serve(&plugin.ServeOpts{
		ProviderFunc: vted.Provider,
	})

	//makeRequest()
}

func makeRequest() {
	client := vted.NewEdClient("https://apps.middleware.vt.edu/ws/v1", "example/ca.crt", "example/client.key", "example/client.crt")
	data, err := client.Get("/groups/research.summit.app.irwin-test?with=all")

	if err != nil {
		log.Fatalln(err)
	}

	pretty, _ := json.MarshalIndent(data.Raw, "", "  ")
	log.Println(string(pretty))
}
